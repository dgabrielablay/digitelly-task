from flask import *
import os
import pymongo
from bson.objectid import ObjectId
from bson.binary import Binary
from bson import BSON
import datetime
from werkzeug.utils import secure_filename
from PIL import Image
from io import BytesIO
import re

UPLOAD_FOLDER = 'static/images'
ALLOWED_EXTENSIONS = ['png', 'jpg', 'jpeg']

app = Flask(__name__)
app.secret_key = 'torontohomes-secret-key'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

# PRE-PROCESSORS

@app.context_processor
def override_url_for():
    return dict(url_for=dated_url_for)

def dated_url_for(endpoint, **values):
    if endpoint == 'static':
        filename = values.get('filename', None)
        if filename:
            file_path = os.path.join(
                app.root_path,
                endpoint,
                filename
            )
            values['q'] = int(os.stat(file_path).st_mtime)
    return url_for(endpoint, **values)


# PAGES

@app.route('/')
def index():
    return render_template('index.html')


if __name__ == "__main__":
    app.run(
        '0.0.0.0',
        debug=True
    )