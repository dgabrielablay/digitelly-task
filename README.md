# Toronto Homes

This project is developed as my first task for Digitelly for my internship program.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software:

* [Git](https://git-scm.com/)
* [Python3.6.x](https://www.python.org/downloads/release/python-3610/)

### Installing

Here's a step by step series of examples that tell you how to get a development env running:

In your terminal, clone the repository

```
git clone https://dgabrielablay@bitbucket.org/dgabrielablay/digitelly-task.git
```

Go into that directory

```
cd digitelly-task
```

Install dependencies

```
pip3 install -r requirements.txt
```

Run the project

```
python3 app.py
```

## Deployment

Should you need to deploy this project in a live system through Heroku or any other hosting site, just simply create a pipeline through this bitbucket link and it should automatically run the code.

## Built With

* [Python Flask](https://flask.palletsprojects.com/en/1.1.x/) - Backend framework used
* [Jinja2](https://jinja.palletsprojects.com/en/2.11.x/) - Frontend client used

## Versioning

We use [Git](https://git-scm.com/) for versioning.

## Authors

* **Don Gabriel Ablay** - *Initial work* - [dgda](https://github.com/dgda)

## Acknowledgments

* Hat tip to [HighViewFilms](https://www.pexels.com/video/a-landmark-in-toronto-2312782/) for the video used as my landing page.
